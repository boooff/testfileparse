import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReadFile {
    public List<String> readFile(String fileName) throws IOException {

        Stream<String> stream = Files.lines (Paths.get (fileName));

        return stream.collect (Collectors.toList ());
    }
}
