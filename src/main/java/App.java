import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class App {
    public static void main(String[] args) throws IOException {
        if (args[0].equals("-h")) {
            printHelp ();
        } else {
            String fileName = args[0];
            int n = Integer.parseInt(args[1]);

            final long startTime = System.nanoTime();

            appRun (fileName, n);

            long duration = System.nanoTime() - startTime;
            System.out.println();
            System.out.println("Program run time: " + TimeUnit.NANOSECONDS.toMillis(duration) + " ms");
        }
    }

    private static void appRun(String fileName, int n) throws IOException {
        List<String> stringsFromfile = readFile (fileName);
        List<Resource> listRes = createResourceList (stringsFromfile);
        Map<String, Double> averageRes = countAverage (n, listRes);
        Map<Integer, Long> histogram = countHistogram (listRes);

        PrintResults.printAverage (averageRes);
        PrintResults.printHistogram (histogram);
    }

    private static List<String> readFile(String fileName) throws IOException {
        ReadFile rf = new ReadFile ();
        return rf.readFile (fileName);
    }

    private static List<Resource> createResourceList(List<String> stringsFromfile) {
        StringParse sp = new StringParse ();

        return stringsFromfile.stream ().
            map (s -> {
                List<String> elementsOfString = sp.stringParseToList (s);
                return sp.createResource (elementsOfString);
            }
        ).collect (Collectors.toList ());
    }

    private static Map<Integer, Long> countHistogram(List<Resource> listRes) {
        CountHistogram ch = new CountHistogram ();
        return ch.countHistogram (listRes);
    }

    private static Map<String, Double> countAverage(int n, List<Resource> listRes) {
        CountAvarage ca = new CountAvarage ();
        Map<String, Double> averageRes = ca.countAvarage (listRes);
        averageRes = ca.cutAndSortResourceAverage (averageRes, n);
        return averageRes;
    }

    private static void printHelp() {
        System.out.println("To run program you should use 2 arguments:");
        System.out.println("1. Filename (path) to parse");
        System.out.println("2. Number of resources with the highest average request duration to show.");
    }
}