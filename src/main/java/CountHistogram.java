import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CountHistogram {
    public Map<Integer, Long> countHistogram (List<Resource> resourceList) {
        return resourceList.stream ().
                map (Resource::getHourRunned).
                collect (Collectors.groupingBy
                        (Function.identity (), Collectors.counting ()));
    }
}