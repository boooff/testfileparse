import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StringParse {
    public List<String> stringParseToList(String str) {
        return new ArrayList<> (Stream.of (str.split (" ")).collect (Collectors.toList ()));
    }

    public Resource createResource(List<String> list) {
        String regExTime = "[\\d][\\d]:[\\d][\\d]:[\\d][\\d],[\\d][\\d][\\d]"; // hh:mm:ss,nnn

        int hourRunned = Integer.parseInt (list.stream ().
                filter (r -> r.matches (regExTime)).
                findFirst ().get ().
                substring (0, 2));

        Optional<String> resIndex = list.stream ().
                filter (r -> r.endsWith ("]")).
                findFirst ();

        int indexName = list.indexOf (resIndex.get ()) + 1;
        String name = list.get (indexName);

        name = name.contains ("&") ?
                name.replaceAll ("[&][\\s\\S]+", "")
                : name.replaceAll ("[?][\\s\\S]+", "");

        double time = Double.parseDouble (list.get (list.size () - 1));

        return new Resource(name, time, hourRunned);
    }
}