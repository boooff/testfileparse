import java.util.Map;

public class PrintResults {
    public static void printAverage (Map<String, Double> sortedResource) {
        System.out.println ();
        for (Map.Entry pair : sortedResource.entrySet ())
            System.out.println (pair.getKey () + " average time: " + pair.getValue ());

        System.out.println ();
    }

    public static void printHistogram(Map<Integer, Long> histogram) {
        Map.Entry<Integer, Long> maxPair = histogram.entrySet ().stream ().max (Map.Entry.comparingByValue ()).get ();
        long max = maxPair.getValue ();

        if ((max/10) < 1)
            System.out.println("Requests per hour (10 stars equal " + max + " request(s)):");
        else
            System.out.println("Requests per hour (1 star equals " + max/10 + " request(s)):");

        for (Map.Entry pair : histogram.entrySet ()) {
            double quant = 10 * (long) pair.getValue () / max;

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < quant; i++)
                sb.append("*");

            System.out.println (pair.getKey () + " h - " + sb);
        }
    }
}