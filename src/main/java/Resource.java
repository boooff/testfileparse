public class Resource {
    private final String name;
    private final double time;
    private final int hourRunned;

    public String getName() {
        return name;
    }

    public double getTime() {
        return time;
    }

    public int getHourRunned() {
        return hourRunned;
    }

    public Resource(String resourceName, double time, int hourRunned) {
        this.name = resourceName;
        this.time = time;
        this.hourRunned = hourRunned;
    }
}
