import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CountAvarage {
    public Map<String, Double> countAvarage (List<Resource> listRes) {
        return listRes.stream ().
                collect (Collectors.groupingBy (
                        Resource::getName,
                                Collectors.averagingDouble
                                        (Resource::getTime)));
    }

    public Map<String, Double> cutAndSortResourceAverage (Map<String, Double> averResourceMap, int numberOfRes) {
        return averResourceMap.entrySet ().stream ().
                sorted ( Map.Entry.<String, Double>comparingByValue ().
                        reversed ()).
                limit (numberOfRes).
                collect (Collectors.toMap (Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
    }
}