import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class CountHistogramTest {

    @Test
    public void countHistogram() {
        List<Resource> listRes = new ArrayList<> ();

        listRes.add (new Resource ("res1", 10.0, 0));
        listRes.add (new Resource ("res1", 20.0, 0));
        listRes.add (new Resource ("res3", 25.0, 1));
        listRes.add (new Resource ("res2", 10.0, 2));
        listRes.add (new Resource ("res2", 30.0, 2));
        listRes.add (new Resource ("res1", 15.0, 2));

        Map<Integer, Long> expected = new HashMap<> ();
        expected.put (0, 2L);
        expected.put (1, 1L);
        expected.put (2, 3L);

        CountHistogram ch = new CountHistogram ();
        Map<Integer, Long> actual = ch.countHistogram (listRes);

        Assert.assertEquals (expected, actual);
    }
}