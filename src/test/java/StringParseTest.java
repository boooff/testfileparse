import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class StringParseTest {
    @Test
    public void stringParseToList () {
        List<String> actual;
        String str = "2015-08-19 00:00:03,376 (http--0.0.0.0-28080-85) [USER:300553344974] /mobilityServices.do?action=SERVICES&msisdn=300445644148&contentId=main_subscription 300553344974 in 10";
//        String str = "2015-08-19 00:00:02,814 (http--0.0.0.0-28080-245) [CUST:CUS5T27233] /substypechange.do?msisdn=300501633574 in 17";
        StringParse sp = new StringParse ();

        actual = sp.stringParseToList (str);

        List<String> expected = new ArrayList<> ();

        expected.add ("2015-08-19");
        expected.add ("00:00:03,376");
        expected.add ("(http--0.0.0.0-28080-85)");
        expected.add ("[USER:300553344974]");
        expected.add ("/mobilityServices.do?action=SERVICES&msisdn=300445644148&contentId=main_subscription");
        expected.add ("300553344974");
        expected.add ("in");
        expected.add ("10");

        Assert.assertEquals (actual, expected);
    }

    @Test
    public void createResource() {
        List<String> list = new ArrayList<> ();

        list.add ("2015-08-19");
        list.add ("00:00:03,376");
        list.add ("(http--0.0.0.0-28080-85)");
        list.add ("[USER:300553344974]");
        list.add ("/mobilityServices.do?action=SERVICES&msisdn=300445644148&contentId=main_subscription");
        list.add ("300553344974");
        list.add ("in");
        list.add ("10");

        StringParse sp = new StringParse ();

        Resource actual = sp.createResource (list);
        Resource expected = new Resource ("/mobilityServices.do?action=SERVICES", 10, 0);

        Assert.assertEquals (actual.getName (), expected.getName ());
        Assert.assertEquals (actual.getTime (), expected.getTime (), 0.01);
        Assert.assertEquals (actual.getHourRunned (), expected.getHourRunned ());
    }
}