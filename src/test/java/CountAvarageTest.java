import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class CountAvarageTest {

    @Test
    public void countAvarage() {
        List<Resource> listRes = new ArrayList<> ();

        listRes.add (new Resource ("res1", 10.0, 0));
        listRes.add (new Resource ("res1", 20.0, 0));
        listRes.add (new Resource ("res3", 25.0, 1));
        listRes.add (new Resource ("res2", 10.0, 1));
        listRes.add (new Resource ("res2", 30.0, 1));
        listRes.add (new Resource ("res1", 15.0, 2));


        CountAvarage ca = new CountAvarage ();
        Map<String, Double> averMap = ca.countAvarage (listRes);

        Map<String, Double> expectedMap = new HashMap<> ();

        expectedMap.put ("res1", 15.0);
        expectedMap.put ("res2", 20.0);
        expectedMap.put ("res3", 25.0);

        Assert.assertEquals (expectedMap, averMap);
    }

    @Test
    public void cutAndSortResourceAverage() {
        Map<String, Double> sourceResMap = new HashMap<> ();

        sourceResMap.put ("res1", 5.0);
        sourceResMap.put ("res3", 10.0);
        sourceResMap.put ("res7", 55.0);
        sourceResMap.put ("res2", 2.0);
        sourceResMap.put ("res9", 20.0);

        Map<String, Double> expectedMap = new TreeMap<> ();

        expectedMap.put ("res7", 55.0);
        expectedMap.put ("res9", 20.0);
        expectedMap.put ("res3", 10.0);

        CountAvarage ca = new CountAvarage ();

        Map<String, Double> actualMap = ca.cutAndSortResourceAverage (sourceResMap, 3);

        Assert.assertEquals (expectedMap, actualMap);

    }
}